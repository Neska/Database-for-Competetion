<?php

use yii\db\Migration;

/**
 * Class m180326_171116_tfo_user
 */
class m180326_171116_tfo_user extends Migration
{
        public function up()
    {
        $this->createTable('tfo_user', [
            'id' => $this->primaryKey(),
            'first_name' => $this->string(30)->notNull(),
            'last_name' => $this->string(30)->notNull(),
            'email' => $this->string(60)->notNull()->unique(),
            'created_at' => $this->date()->notNull(),
            'password_hash' => $this->string()->notNull(),
            'status' => $this->string(20)->notNull()
        ]);
        
         $this->insert('tfo_user', [
          
            'first_name' => 'Neska',
            'last_name' => 'Spahic',  
            'email' => 'neskaspahic1997@gmail.com',
            'created_at' => date('Y-m-d H:i:s'),
            'password_hash' => Yii::$app->security->generatePasswordHash ('neska123'),
            'status' => 'ACTIVE', 
        ]);
}
    public function down()
    {
        echo "m180316_202411_tfo_user cannot be reverted.\n";

        return false;
    }
}
