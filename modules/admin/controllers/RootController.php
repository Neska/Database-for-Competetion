<?php

namespace app\modules\admin\controllers;

use Yii;
use yii\helpers\Url;
use yii\web\Controller;
use yii\filters\AccessControl;
use app\modules\admin\models\LoginForm;
use app\modules\admin\models\TfoUser;

class RootController extends Controller
{
    public $defaultAction = 'login';
	
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

	 public function actionLogin()
     {
        $model = new LoginForm();
        

        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            $user = TfoUser::find()->where(['email' => $model->email])->one();
            if ($user->status=='ACTIVE')
                return $this->redirect(Url::toRoute('administrator/index'));
        }
        
        return $this->render('login', ['model' => $model,]);  
    }
	
	
    public function actionLogout(){
		
        Yii::$app->user->logout();

        return $this->redirect(Url::toRoute('login'));
	}
}
