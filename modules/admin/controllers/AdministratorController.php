<?php
namespace app\modules\admin\controllers;

use yii\data\ActiveDataProvider;
use yii\helpers\Url;
use yii\web\Controller;
use app\modules\admin\models\User;
use Yii;

class AdministratorController extends Controller
{
    /**
     * List all administrators.
     *
     * @return string
     */
    
    public function actionIndex()
    {

        $dataProvider = new ActiveDataProvider([
            'query' => User::find()->where(['!=','status', "DELETED" ])
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider
        ]);

    }

    public function actionCreate() {

        $model = new User();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(Url::toRoute('index'));
        }

        return $this->render('create', [
            'model' => $model
        ]);
    }

    public function actionUpdate($id) {
      
        $model = User::findOne($id);

        if($model->load(Yii::$app->request->post()) && $model->save()) {
                return $this->redirect(Url::toRoute('index'));
        }

        return $this->render('update', [
            'model' => $model
        ]);
    }

    public function actionUpdatePass($id) {

        $model = User::findOne($id);
        $model->password_hash = "";

        if($model->load(Yii::$app->request->post())) {
            $model->password_hash = Yii::$app->security->generatePasswordHash ($model->password_hash);
            if ($model->save())
                return $this->redirect(Url::toRoute('index'));
        }

        return $this->render('update_password', [
            'model' => $model
        ]);
    }

    public function actionDelete($id)
    {
        $model = User::findOne($id);
        $model->status='DELETED';
        $model->save();
        /*

        /*if (!is_null($model)) {
            $model->delete();
        }*/

        return $this->redirect(Url::toRoute('index'));
    }
}

