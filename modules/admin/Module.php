<?php

namespace app\modules\admin;

class Module extends \yii\base\Module
{
    public $defaultRoute = 'root';

    public $layout = 'main';
}
