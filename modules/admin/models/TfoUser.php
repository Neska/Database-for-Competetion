<?php

namespace app\modules\admin\models;

use yii\db\ActiveRecord;

/**
 * Class TfoUser
 * @package app\modules\admin\models
 *
 * @property int $id
 * @property string $email
 * @property string $first_name
 * @property string $last_name
 * @property string $created_at
 */
class TfoUser extends ActiveRecord
{

    // @return string the name of the table associated with this ActiveRecord class.
    public static function tableName()
    {
        return 'tfo_user';
    }

    public function rules()
    {
        return [
            [['first_name', 'last_name', 'email','password_hash','status'], 'required', 'message' => 'popuni polje'],
            [['email'], 'email'],
            [['email'], 'unique']
        ];
    }

    public function beforeSave($insert)
    {
        $result = parent::beforeSave($insert);

        if ($this->isNewRecord) {
            $this->password_hash = \Yii::$app->getSecurity()->generatePasswordHash($this->password_hash);
            $this->created_at = date('Y-m-d');
        }

        return $result;
    }
}

?>