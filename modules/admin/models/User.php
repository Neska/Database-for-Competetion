<?php

namespace app\modules\admin\models;
use Yii;
use yii\base\NotSupportedException;
use yii\db\ActiveRecord;
use yii\helpers\Security;
use yii\web\IdentityInterface;



class User extends \yii\db\ActiveRecord  implements IdentityInterface {


public static function tableName() {
    return 'tfo_user';
}

/**
 * @inheritdoc
 */
 public function rules()
    {
        return [
            [['first_name', 'last_name', 'email','password_hash','status'], 'required', 'message' => 'popuni polje'],
            [['email'], 'email'],
            [['email'], 'unique']
        ];
    }


public static function findIdentity($id) {
    return static::findOne($id);
}

/* modified */
public static function findIdentityByAccessToken($token, $type = null) {
    return static::findOne(['access_token' => $token]);
}

public static function findByEmail($email) {
    return static::findOne(['email' => $email]);
}

public function getId() {
    return $this->getPrimaryKey();
}

public function validatePassword($password)
{
    return Yii::$app->getSecurity()->validatePassword($password, $this->password_hash);
}

public function beforeSave($insert)
    {
        $result = parent::beforeSave($insert);

        if ($this->isNewRecord) {
            $this->password_hash = \Yii::$app->getSecurity()->generatePasswordHash($this->password_hash);
            $this->created_at = date('Y-m-d');
        }

        return $result;
    }
/*
public function setPassword($password)
{
    $this->password_hash = Yii::$app->security->generatePasswordHash($password);
}

public function beforeSave($insert) {
    if(isset($this->password_hash)) 
        $this->password_hash = Security::generatePasswordHash($this->password_hash);
    return parent::beforeSave($insert);
}
*/
//Authentication 

public function getAuthKey() {
    //return $this->authKey;
}
public function validateAuthKey($authKey) {
    //return $this->getAuthKey() === $authKey;
}

/*
public static function findByPasswordResetToken($token) {
    $expire = \Yii::$app->params['user.passwordResetTokenExpire'];
    $parts = explode('_', $token);
    $timestamp = (int) end($parts);
    if ($timestamp + $expire < time()) {
        // token expired
        return null;
    }

    return static::findOne([
                'password_reset_token' => $token
    ]);
}

public function generatePasswordResetToken() {
    $this->password_reset_token = Security::generateRandomKey() . '_' . time();
}

public function removePasswordResetToken() {
    $this->password_reset_token = null;
}*/

}