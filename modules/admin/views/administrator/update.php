<?php
/**
 * @Copyright 2018, DSO Gradient, All rights reserved
 */
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var $model \app\modules\admin\models\TfoUser
 */
?>

<?php $form = ActiveForm::begin(); ?>

<?= $form->field($model, 'first_name')?>
<?= $form->field($model, 'last_name')?>
<?= $form->field($model, 'email')?>

<?= $form->field($model, 'status')->dropdownList([
        'ACTIVE' => 'ACTIVE', 
        'INACTIVE' => 'INACTIVE',
        'DELETED' => 'DELETED'
    ],
    ['prompt'=>'STATUS  ']
);
?>

<?= Html::submitButton('Save', ['class' => 'btn btn-success', 'style' => ' width: 100px']) ?>
<?= Html::a('Change Password', \yii\helpers\Url::toRoute(['update-pass', 'id' => $model->id]), [
    'class' => 'btn btn-primary',
    'style' => 'margin-left: 20px; width: 200px'
]); ?>
<?= Html::a('Back', \yii\helpers\Url::toRoute('index'), [
    'class' => 'btn btn-primary',
    'style' => 'margin-left: 20px; width: 100px'
]); ?>

<?php ActiveForm::end(); ?>
