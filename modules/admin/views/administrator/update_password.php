<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;


?>

<?php $form = ActiveForm::begin(); ?>

<?= $form->field($model, 'password_hash')->passwordInput()->label('Password')?>

<?= Html::submitButton('Save', ['class' => 'btn btn-success', 'style' => ' width: 100px']) ?>
<?= Html::a('Back', \yii\helpers\Url::toRoute('index'), [
    'class' => 'btn btn-primary',
    'style' => 'margin-left: 20px; width: 100px'
]); ?>

<?php ActiveForm::end(); ?>
