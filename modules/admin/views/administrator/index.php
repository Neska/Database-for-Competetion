<?php
/**
 * @Copyright 2018, DSO Gradient, All rights reserved
 */
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * @var $dataProvider \yii\data\ActiveDataProvider
 */
?>

<h1>Administrators</h1>

<div class="pull-right">
    <?= Html::a("Create", Url::toRoute('create'), [
            'class' => 'btn btn-primary',
            'style' => 'margin-bottom: 10px; width: 200px'
    ]); ?>
</div>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => [
        'id',
        'first_name',
        'last_name',
        'email',
        'created_at:date',
        'status',
        [
            'class' => 'yii\grid\ActionColumn',
            'template'=>'{update}{delete}',
            'header'=>"Update and Delete"
        ],
    ],
]);