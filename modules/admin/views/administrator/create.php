<?php
/**
 * @Copyright 2018, DSO Gradient, All rights reserved
 */
use yii\web\View;

/**
 * @var $this View
 */

/**
 * @var $model \app\modules\admin\models\TfoUser
 */
?>

<h1> Create administrator </h1>

<?= $this->render('_form', [
    'model' => $model
]); ?>
